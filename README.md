# Nose Ears website

This is the source code for the Nose Ears website, a webcomic by Wuzzy.

## Building

This is a static website, and you need [Hugo](https://gohugo.io) to generate it.

Just run the `hugo` command in the website directory and get the finished website from the `public` directory. Now it's ready to be uploaded on basically any webhost.

## Comics
Comic metadata is stored in the `.csv` files, which are CSV files compliant with RFC 4180
<https://datatracker.ietf.org/doc/html/rfc4180>.
Run `python csv_to_content.py` to generate the respective comic Markdown files.

**NOTE**: This repository does NOT include the comic files itselves! They have to be added into `assets/comics` manually.

## License

[CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).
