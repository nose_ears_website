+++
title = "Statistiken"
date = "2022-02-02"
+++
## Nasenohren
Statistik über *Nasenohren*, der deutschen Version des Comics.

* Anzahl der Comics: **{{< countcomics lang="de" >}}**
* Davon ohne Übersetzung: **{{< countexclusive lang="de" >}}**
* Anzahl der [Themen]({{< ref "/topics" >}}): **{{< counttopics >}}**
* Anzahl der [Serien]({{< ref "/series" >}}): **{{< countseries >}}**
* Meiste Themen eines Comics: **{{< mosttopics >}}**
* Erster Comic veröffentlicht am: **{{< comicdate "first" >}}**
* Letzter Comic veröffentlicht am: **{{< comicdate "last" >}}**

## Englische Version
Statistik über die [englische Version]({{< ref path="/meta/stats" lang="en" >}}) des Comics (*Nose Ears*).

* Anzahl der Comics auf Englisch: **{{< countcomics lang="en" >}}**
* Davon ohne Übersetzung: **{{< countexclusive lang="en" >}}**

## Globale Statistik
Diese Werte beziehen sich auf alle Comics über alle Sprachversionen hinweg.

* Anzahl der Comics in allen Sprachen: **{{< countcomics >}}**
* Letzte Comic-ID: **#{{< lastcomicid >}}**
* Größe aller Comicbilddateien (ohne Vorschau): **{{< comicfilesize >}}**
