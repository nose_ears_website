+++
title = "Figuren"
date = "2024-01-30"
+++
Die Hauptfiguren sind schneeweiße kreisrunde kleine Kreaturen. Sie haben eine Nase, Ohren, Augen und Füße. Sie gehören einer unbekannten Spezies an, aber sie sind ganz eindeutig keine Menschen.

## Identität

Ihre Identität, Rolle und Stimmung ändern sich dramatisch die ganze Zeit. Sie verkörpern das Wesen des **Chaos**, sind unbeschriebene Blätter, die der Sinnhaftigkeit Hohn sprechen. An einem Tag arbeiten sie vielleicht als [Gefängniswärter]({{< ref "/comic/8" >}}) oder für die [Konzern-AG]({{< ref "/comic/25" >}}) und an einem anderen Tag werden sie zu tapferen Kriegern im Kampf gegen den [wahren Feind]({{< ref "/comic/30" >}}), und an einem anderen Tag [streiten sie nur darüber, wer den Kuchen gegessen hat]({{< ref "/comic/64" >}}). Wie Menschen [lieben sie]({{< ref "/comic/71" >}}), [lachen sie]({{< ref "/comic/70" >}}), [sind niedergeschlagen]({{< ref "/comic/23" >}}), [weinen]({{< ref "/comic/62" >}}).

Sicher ist jedoch, dass sie zu so ziemlich allem eine Meinung haben, nur um am nächsten Tag eine 180°-Wendung zu machen. Sie machen sich oft über schlechte Ad-hoc-Argumente lustig (oder über Argumente, die *scheinbar* schlecht sind). Meistens streiten sie sich, aber sie finden kurz darauf wieder Frieden. Ihr Beziehungsstatus ist genauso chaotisch wie ihre Identität.

## Liste der Figuren

### Mimi
![Mimi](/assets/images/mimi.png)

* Ohrenform: spitzohrig
* Gender: chaotisch
* Pronomen: *beliebig*
* Erster Auftritt: [Hass]({{< ref "/comic/1" >}})

### Eumel
![Eumel](/assets/images/eunice.png)

* Ohrenform: schlabberohrig
* Gender: chaotisch
* Pronomen: *beliebig*
* Erster Auftritt: [Hass]({{< ref "/comic/1" >}})

### Poppi
![Poppi](/assets/images/poppy.png)

* Ohrenform: [wechselohrig]({{< ref "/comic/123" >}})
* Gender: weiblich
* Pronomen: sie
* Erster Auftritt: [Ohrenbinärität]({{< ref "/comic/53" >}})

### Keno
![Keno](/assets/images/keno.png)

* Ohrenform: knickohrig
* Gender: männlich
* Pronomen: er
* Erster Auftritt: [Ohrenternärität]({{< ref "/comic/123" >}})

## Grammatik

Manche fragen sich vielleicht: „Was bedeutet: ‚Pronomen: beliebig‘?“. Nun, es bedeutet, dass die Pronomen, die zum Verweis auf die Person verwendet werden können, alles sein können, was grammatikalisch korrekt ist. Also kann es „er“, „sie“ oder sogar „es“, „ser“, „xer“, oder was auch immer sonst noch als Pronomen heute verwendet werden kann.

Zum Beispiel bedeuten all diese Sätze das gleiche:

* „Mimi ist ein Vertreter des Chaos. *Ihr* ist es egal.“
* „Mimi ist ein Vertreter des Chaos. *Ihm* ist es egal.“
* „Mimi ist ein Vertreter des Chaos. *Ser* ist es egal.“
* „Mimi ist ein Vertreter des Chaos. *Xer* ist es egal.“
* (und so weiter)

Aber das Identitätschaos hat eine Grenze: Sobald ein Pronomen gewählt wurde, wird sich das Pronomen einer Figur niemals während eines einzelnen Comics ändern. ;-)

Poppi und Keno sind anders, denn ihre Pronomen sind klar definiert. Es ist immer „er“ für Keno und immer „sie“ für Poppi.

## Die Welt von Nasenohren

### Konzern-AG
![Logo der Konzern-AG](/assets/images/corporation_inc_logo_de.png)

Die **Konzern-Aktiengesellschaft** ist ein riesiges gesichtsloses Multi-Quadrillionen-Unternehmen, das danach strebt, den Markt und damit die Welt zu beherrschen. Sie sind vor allem als Software- und Internetunternehmen tätig, aber als riesiges gesichtsloses Multi-Quadrillionen-Unternehmen versuchen sie gelegentlich, auch in andere Märkte einzudringen. Ihr Logo ist der Großbuchstabe K.

### Währung

Die offizielle Währung heißt „Moneten“. Die Einzahl lautet „Monete“. Das Währungssymbol ist der Großbuchstabe M mit je einem Strich an der linken und rechten Seite.

## Weitergehende Informationen

* [Über]({{< ref "/meta/about" >}})
