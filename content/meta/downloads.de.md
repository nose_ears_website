+++
title = "Downloads"
date = "2023-08-09"
+++

## Archiv

Im [Nasenohren-Archiv](https://archive.org/details/nose-ears-archive) (Seite auf Englisch) gibt es die alten Comics in einer großen Zip-Datei. Das ist nur ein Backup für Notfälle und enthält nicht immer die neuesten Comics.

## Schrift

Nasenohren benutzt seine eigene Schrift, siehe [Schrift]({{< ref "/meta/font" >}}).
