+++
title = "Making-of"
date = "2021-11-09"
+++
Die Erstellung der Comics ist einfach, aber höchst effizient.

Da ich nicht gut zeichnen kann, habe ich eine Bibliothek mit vorgezeichneten Mimi- und Eumel-Figuren angelegt. Die Bibliothek wurde erstellt, indem viele Charakterzeichnungen aus den „Mimi and Eunice“-Comics (die [freie kulturelle Werke](https://freedomdefined.org/Definition) sind) extrahiert wurden, die Mimi und Eumel in verschiedenen emotionalen Zuständen zeigen, von denen jeder mehrere Varianten hat, um offensichtliche Wiederholungen zu vermeiden. Der Aufbau dieser Bibliothek war eine mühsame Arbeit, aber sie hat sich gelohnt, da sie die Erstellung neuer Comics massiv beschleunigt.

Für den Text verwende ich meist die „Nizzy“-[Schrift]({{< ref "/meta/font" >}}), eine Abwandlung der „Nina“-Schriftfamilie von Nina Paley; manchmal verwende die Nina-Schriftfamilie direkt. Ich habe die Schrift selbst bearbeitet, hauptsächlich, um neue Glyphen hinzuzufügen.

Der erste Schritt eines jeden Comics besteht darin, sich einen Witz oder ein Szenario auszudenken. Und dann geht es ans eigentliche Erstellen. Um den Comic zu erstellen, starte ich [GIMP](https://www.gimp.org), lade eine der verschiedenen Vorlagen, die ich erstellt habe, schreibe den Text, zeichne Sprechblasen und setze dann die Figuren ein. Manchmal füge ich noch ein paar kleine Bilder hinzu, die meist auf gemeinfreien Bildern basieren. Aber das ist noch nicht alles. Für jeden Comic muss ich auch eine Reihe von Metadaten schreiben. Titel, Themen, Übersetzung, eine Transkription für Leute, die keine Bilder sehen können, usw.

## Verwendete Tools

Nun, was soll ich sagen? Ich stehe in der Tat auf den Schultern von Giganten. Ich bin ein großer [Freie-Software](http://de.wikipedia.org/wiki/Freie_Software)-Nerd. Die Erstellung dieses Webcomics involviert jede Menge freie Software. Hier ist eine Liste von Software (alles *freie Software*), die ich ständig für die Erstellung dieses Comics verwende:

* **Comic-Bilder**
    * [GIMP](https://www.gimp.org), ein Bildbearbeitungsprogramm. Dies ist das Hauptprogramm für die Erstellung der Comics. Ich habe eine Menge Erfahrung mit diesem Programm, da ich es schon seit Ewigkeiten benutze
    * [Inkscape](https://inkscape.org), ein Editor für Vektorgrafiken. Ich benutze es selten, um verschiedene gemeinfreie Vektorgrafiken so zu bearbeiten, dass sie besser zu meinen Comics passen, und sie dann in GIMP zu importieren
    * [Blender](https://www.blender.org), ein 3-D-Modellierungsprogramm. Dieses Programm habe ich für *[Weltumsegelung]({{< ref "/comic/372" >}})* benutzt
* **Textlogo**
    * Inkscape
* **Schrift**
    * [FontForge](https://fontforge.org), ein mächtiger Schriften-Editor. Ich habe ihn benutzt, um neue Glyphen zu zeichnen.
* **Webpräsenz**
    * [Hugo](https://gohugo.io), ein Statische-Webseiten-Generator. Erklärung für Laien: Dies ist ein Werkzeug, das mir hilft, die Erstellung jeder Seite dieser Webpräsenz zu automatisieren. Ohne es müsste ich jede einzelne Seite von Hand erstellen und immer wieder kopieren. Da diese Webpräsenz Hunderte von Seiten umfasst, kannst du dir denken, wie schnell es unmöglich wäre, sie zu verwalten. Hugo ist sehr wichtig.
    * [Git](https://git-scm.com), ein Werkzeug zur Versionskontrolle, das Lieblingswerkzeug eines jeden Programmierers. Erklärung für Laien: Dies ist ein Werkzeug, das die Änderungen an allen wichtigen Dateien, die ich (für die Webpräsenz) mache, verfolgt, wie in einem riesigem Änderungsprotokoll. Wenn ich jemals in meinem Code Mist baue, kann ich das jederzeit zurücknehmen.
* **Schreiben**
    * [Vim](https://www.vim.org) (meistens), ein Retro-Texteditor, weil ich auch retro bin.
* **Hosting**
    * Diese Webpräsenz wird bei [Neocities](https://github.com/neocities) gehostet, einem Hosting-Service für Webpräsenzen. Neocities selbst hat ihre gesamte Software, die sie verwenden, als freie Software veröffentlicht, weil sie einfach großartig sind!

## Geschichte

Die ersten Comics stammen aus dem Jahr 2014, als ich meine ersten Versuche mit Comic-Bearbeitungen unternahm. Diese frühen Comics wurden nicht veröffentlicht, aber die überarbeiteten Versionen von ihnen schon. Dann passierte viele Jahre lang so gut wie nichts.

Im Oktober 2021 beschloss ich schließlich, aus meinem Experiment etwas Ernsthaftes zu machen, und so wurde Nasenohren geboren. Mir war halt einfach danach. Viele neue Comics entstanden, Übersetzungen wurden getippt, die Webpräsenz wurde erstellt, Übersetzungen wurden angefertigt, und vieles mehr.

Am 24. Oktober 2021 ging die Nasenohren-Webpräsenz an die Öffentlichkeit.

Und das ist in etwa die Entstehungsgeschichte von Nasenohren. Für die lange Geschichte siehe [Lange Geschichte]({{< ref "/meta/long_history" >}}).

