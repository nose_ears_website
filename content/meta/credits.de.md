+++
title = "Mitwirkende"
date = "2021-10-25"
+++
**Nasenohren** ist ein Webcomic, der von Wuzzy kreiert wurde. (siehe auch: [Wuzzys persönliche Webpräsenz](https://wuzzy.codeberg.page) (auf Englisch))

Bitte kopieren und teilen! ♡ Ich möchte, dass sich dieser Comic verbreitet.

Nasenohren ist ein [freies kulturelles Werk](https://freedomdefined.org/Definition).
Du wirst dazu ermutigt, ihn zu kopieren und weiterzugeben, davon abgeleitete Werke zu erstellen. Ob kommerziell oder
oder nicht, solange du die Namensnennung einhältst und nicht gegen die
Copyleft-Bedingung der Lizenz verstößt, ist das in Ordnung.

## Mitwirkende

* Geschichten, Text: Wuzzy
* Englische und deutsche Sprachversionen: Wuzzy
* Webpräsenz: Wuzzy
* Text-Logo: Wuzzy (unter Verwendung der Schriftfamilie „Nina“ von Nina Paley)
* Figurenzeichnungen: Nina Paley (manchmal mit Änderungen von Wuzzy, siehe unten)
* Andere Bilder: Anonyme Autoren (gemeinfreie Quellen), Nina Paley, Wuzzy
* [Schrift]({{< ref "/meta/font" >}}): Nina Paley, Wuzzy

## Lizenz / rechtliche Informationen

Nasenohren ist lizenziert unter **CC BY-SA 3.0**, d.h. 
[Namensnennung - Weitergabe unter gleichen Bedingungen 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/deed.de).

[Comic #57]({{< ref "/comic/57" >}}) verwendet freie Logos von diversen freien Softwareprojekten.
Siehe [Mitwirkende für Comic #57]({{< ref "/meta/credits_57" >}}) für Details.

[Comic #263]({{< ref "/comic/263" >}}) verwendet ein Bild der Europäischen Südsternwarte (ESO).
Siehe [Mitwirkende für Comic #263]({{< ref "/meta/credits_263" >}}) für Details.

Nasenohren ist ein von dem Webcomic Mimi and Eunice von Nina Paley abgeleitetes Werk.
Mimi and Eunice ist ebenfalls lizenziert unter CC BY-SA 3.0. Nasenohren
enthält sowohl die titelgebenden Figuren Mimi und Eunice (Eunice heißt in Nasenohren „Eumel“)
als auch die Zeichnungen dieser Figuren. Andere Figuren wie Poppi und Keno sind
neu und basieren auf den „Mimi and Eunice“-Zeichnungen. Die Geschichten und Texte in Nasenohren
sind völlig neu.

Andere Zeichnungen und Bilder sind eine Mischung aus Bildern, die vom „Mimi and Eunice“-Webcomic
abgeleitet wurden, Bildern aus diversen anonymen gemeinfreien Quellen und selten auch Wuzzys
eigenen Werken.

Obwohl die Arbeit von Nina Paley für Nasenohren verwendet wird, ist sie *nicht* aktiv an der
an der Erstellung von Nose Ears beteiligt. Nasenohren entsteht völlig unabhängig von
von Mimi and Eunice. Es gibt weder eine direkte Zusammenarbeit noch eine Befürwortung.

Außerdem wird die Schriftfamilie „Nina“ von Nina Paley für die Texte verwendet (einschließlich des Textlogos),
lizenziert unter CC BY-SA 3.0. Die Schriftfamilie wurde bezogen von
[archive.org](https://archive.org/details/NinaPaleyFonts9).
Für die meisten Comics verwende ich jedoch mittlerweile eine bearbeitete Version der Schrift, siehe [Schrift]({{< ref "/meta/font" >}}).

### Beweise für erworbene Lizenzen

Nachweis der Lizenz für Mimi and Eunice:

* [Mimi and Eunice: „About“-Seite](https://mimiandeunice.com/about/)
* [Archivierte Seite davon](https://web.archive.org/web/20210809081608/https://mimiandeunice.com/about/)
* [Archivierter Screenshot](/assets/images/Mimi_and_Eunice_permission_CC_BY_SA_3_0.png) (aufgenommen am 18. Oktober 2021)

Lizenznachweis für die Schriftfamilie Nina:

* [Infoseite auf archive.org](https://archive.org/details/NinaPaleyFonts)
* [Archivierter Screenshot](/assets/images/Nina_font_permission_CC_BY_SA_3_0.png) (aufgenommen am 18. Oktober 2021)
