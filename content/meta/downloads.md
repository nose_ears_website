+++
title = "Downloads"
date = "2023-08-09"
+++

## Archive

The [Nose Ears Archive](https://archive.org/details/nose-ears-archive) contains past comics in one large Zip file. This is basically an emergency backup and may not always contain the latest comics.

## Font

Nose Ears uses its own font, see [Font](/meta/font).
