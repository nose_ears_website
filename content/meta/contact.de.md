+++
title = "Kontakt"
date = "2021-10-25"
+++

So kann man mich, Wuzzy, kontaktieren:

* **E-Mail**: [Wuzzy@disroot.org](mailto:Wuzzy@disroot.org)
* **XMPP**: [Wuzzy2\@jabber.ccc.de](xmpp:Wuzzy2@jabber.ccc.de)
* **Mastodon**: [@Wuzzy@cyberplace.social](https://cyberplace.social/@Wuzzy)
* **Matrix**: \@wuzzy:matrix.org
* **IRC**: Wuzzy auf [Libera.Chat](ircs://irc.libera.chat:6697)

Ich habe auch eine [persönliche Webpräsenz](https://wuzzy.codeberg.page) (auf Englisch).
