+++
title = "Unterstütze diesen Comic!"
date = "2021-10-25"
+++

Magst du diesen Comic sehr? So kannst du ihn unterstützen:

## Unterstützung mit Sichtbarkeit

Der einfachste Weg, diesen Comic zu unterstützen, ist, ihn einfach **weiterzuempfehlen**. Erzähl es deinen Freunden, poste Links, poste ein paar Comics in Diskussionsforen, im Chat, druck sie aus, sei kreativ. Kunst wird geschaffen, um gesehen zu werden, also je mehr dies sehen, desto besser. Mach dir keine Sorgen um das Copyright, denn dieser Comic ist ein freies kulturelles Werk. Siehe [hier]({{< ref "/meta/credits" >}}) für Details.

### Kein Hotlinken!

Beachte, dass Hotlinking (d. h. das direkte Verlinken der Bilddatei, um sie in eine Webseite einzubetten) derzeit *nicht* unterstützt wird. Wenn du einen Comic weitergeben möchte, gib entweder den Link weiter oder kopiere das Bild direkt.

## Bei Top Web Comics abstimmen

Nasenohren wird bei den englischsprachigen [Top Web Comics](https://www.topwebcomics.com/) gelistet. Du kannst [hier](https://www.topwebcomics.com/vote/27297?userid=Wuzzy) alle 24h eine Stimme abgeben (auf „Vote“ klicken), damit der Comic in der Rangliste nach oben klettert.


