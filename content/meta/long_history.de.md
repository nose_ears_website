+++
title = "Lange Geschichte"
date = "2021-10-25"
+++
So ist Nasenohren entstanden.

## Mimi and Eunice
Ich war schon immer ein großer Fan von Mimi and Eunice von Nina Paley, einem meiner Lieblings-Webcomics. Ich mag die einfache Struktur und die Tatsache, dass es einer der wenigen Webcomics ist, der die Argumente der Urheberrechtsmaximalisten direkt angreift. Ich verfolge ihn seit Jahren und ohne ihn gäbe es Nasenohren nicht. Nina Paley gebührt dafür natürlich viel Anerkennung, obwohl sie zu keinem Zeitpunkt direkt an der Entstehung von Nasenohren beteiligt ist.

## 2014: Frühe Comics
Technisch gesehen geht Nasenohren auf Ende 2014 zurück, als ich anfing, die ersten Comics zu machen, die von Mimi and Eunice abgeleitet waren. Ich habe mit dem „Mimi and Eunice“-Webcomic herumexperimentiert; außerdem war ich damals von Lojban besessen, also habe ich die ersten Comics in Lojban gezeichnet und erst später englische Versionen davon gemacht. Die Comics [#1]({{< ref "/comic/1" >}}) bis [#18]({{< ref "/comic/18" >}}) (außer [#9]({{< ref "/comic/9" >}})) entstanden ursprünglich in dieser Zeit. Die Methode, mit der ich die Comics damals gemacht habe, war etwas anders: Ich habe die Buchstaben selbst gezeichnet. Diese Praxis habe ich inzwischen aufgegeben, da sie zu mühsam ist. Jetzt verwende ich einfach eine Computerschrift.

Auch meine „Bibliothek“ von Figurenzeichnungen (Bilder von Mimi und Eunice=Eumel) stammt noch aus den Anfängen. Da es über 500 „Mimi and Eunice“-Comics zur Auswahl gibt, habe ich viele „Mimis“ und „Eumels“, aus denen ich neue Comics erstellen kann. Das ist ein einfacher, aber genialer Trick, um jeden Comic einzigartig erscheinen zu lassen. Ich möchte vermeiden, dass sich identische Figurenzeichnungen wiederholen oder zumindest zu offensichtlich werden. Es ist mir wichtig, dass die Figuren in verschiedenen Emotionen und Varianten erscheinen.

## 2014-2021: Die Ära des Nichts
Dann schlummerten meine Comics jahrelang auf meiner Festplatte und es passierte so gut wie nichts. Ab und zu hatte ich die Idee, die Comics einfach irgendwo hochzuladen, aber ohne eine richtige Webpräsenz wäre das Beste, was ich hätte machen könnte, nur eine langweilige Liste gewesen. Das war mir nicht gut genug.

In der Zwischenzeit ist der „Mimi and Eunice“-Comic im Grunde gestorben (für *mich* jedenfalls), seit Jahren nicht mehr aktualisiert, und die letzten Comics sind sehr transphob geworden, indem eine neue Figur eingeführt wurde, der im Grunde ein sehr schlechtes Klischee war. Vielleicht schreibe ich später mehr darüber, *warum* genau ich denke, dass diese Comics transphob sind, weil es für Laien nicht offensichtlich ist.

Dennoch denke ich, dass Mimi and Eunice insgesamt ein brillanter Webcomic ist, der auf elegante Weise das Urheberrechtssystem entlarvt. Das ist der Bereich, in dem dieser Webcomic glänzt. Wenn es um Gender geht, ist er Müll. Mimi and Eunice war nicht von Anfang an transphob, es wurde erst ganz am Ende transphob.

## Oktober 2021: Nasenohren beginnt!
Im Oktober 2021 habe ich dann beschlossen, neue Comics mit den Figuren von Mimi and Eunice zu machen. Ich weiß nicht wirklich, *warum* ich mich dazu entschlossen habe, ich glaube, es war mir halt einfach danach. ;-)

Die erste naheliegende Idee war, einige trans-positive Comics zu machen, um dem transphoben Unsinn direkt entgegenzutreten, so wie Nina die irrationale Copyright-Logik so gekonnt entlarvt hat. Deshalb gibt es am Anfang auch viele Comics über Queerness. Aber Nasenohren soll mehr sein als das. Ich werde alle möglichen Themen behandeln, die mich interessieren, hauptsächlich Computer, freie Software, ungerechte Machtstrukturen und mich über absurde Argumente lustig machen.

Also begann ich, eine Menge neuer Comics zu erstellen und war in nur etwa einer Woche sehr produktiv. In diesem Monat habe ich auch die wunderschöne Nasenohren-Webpräsenz mit [Hugo](https://gohugo.io/), einem Generator für statische Webpräsenzen, von Grund auf neu erstellt.

Außerdem habe ich alle meine alten Comics aus dem Jahr 2014 überarbeitet, weil sie mich noch nicht glücklich gemacht haben. Da diese Comics noch nirgends veröffentlicht waren, war das in Ordnung. Außerdem habe ich viel Arbeit in die Erweiterung und Überarbeitung meiner Figurenbibliothek gesteckt. Viele der alten Figurenzeichnungen, die ich extrahiert habe, hatten ärgerliche Fehler, die ich nicht tolerieren konnte, also habe ich angefangen, sie zu korrigieren.

Ich habe auch deutsche Pendants zu den Comics erstellt. Dafür reicht die von mir verwendete [Schrift]({{< ref "/meta/font" >}})familie „Nina“ nicht aus, da sie die zusätzlichen deutschen Zeichen, die ich brauche (Umlaute, Eszett, Anführungszeichen), nicht unterstützt.

Außerdem musste ich mir einen neuen Namen ausdenken. „Mimi und Eunice 2“ kam mir zuerst in den Sinn, aber das musste ich wieder verwerfen. Mein Comic ist nicht als Fortsetzung gedacht, sondern als etwas eigenständiges. Außerdem würde der Name des anderen Comics im Titel nur falsche Assoziationen wecken. Ich brauchte also einen völlig neuen, einzigartigen Namen, und nach einigem Nachdenken habe ich „Nasenohren“ (bzw. „Nose Ears“ auf Englisch) genommen. Es heißt „Nasenohren“, weil das die beiden charakteristischen Körperteile von Mimi und Eumel sind. Und es klingt einfach sehr lustig, gerade in der deutschen Version.

Und schließlich, am 24. Oktober 2021, ging die Nasenohren-Webpräsenz mit 66 Comics an den Start.

## Weitere Ereignisse

* 2\. Februar 2022: [Statistikseite]({{< ref "/meta/stats" >}}) hinzugefügt
* 3\. April 2023: Unterstützung für kleine Bildschirme hinzugefügt
* 7\. August 2023: Comic-Liste zeigt ab heute auch die Comic-ID und das Datum
* 7\. September 2023: [Archiv](https://archive.org/details/nose-ears-archive) veröffentlicht
* 2\. Februar 2024: Nasenohren wurde hinzugefügt zu: [Top Web Comics](https://www.topwebcomics.com/sample/27297), [Comic Rocket](https://www.comic-rocket.com/explore/nose-ears/), [Belfry Comics](https://new.belfrycomics.net/), [Webcomic-Verzeichnis](http://www.webcomic-verzeichnis.de/comic.php?comic=440), [Piperka](https://piperka.net/info.html?cid=10205) und [Archive Binge](https://archivebinge.com/comic/1733)
* 19\. Februar 2024: Themenliste unter dem Comic hat jetzt Links für den jeweils nächsten und vorherigen Comic in der jeweiligen Kategorie
* 19\. Februar 2024: Einige Comics in [Serien](/series) organisiert
