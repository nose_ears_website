+++
title = "Statistics"
date = "2022-02-02"
+++
## Nose Ears
Statistics about *Nose Ears*, the English version of the comic.

* Number of comics strips: **{{< countcomics lang="en" >}}**
* Comic strips without translation: **{{< countexclusive lang="en" >}}**
* Number of [topics]({{< ref "/topics" >}}): **{{< counttopics >}}**
* Number of [series]({{< ref "/series" >}}): **{{< countseries >}}**
* Most topics of a comic strip: **{{< mosttopics >}}**
* First comic strip published at: **{{< comicdate "first" >}}**
* Last comic strip published at: **{{< comicdate "last" >}}**

## German version
Statistics about the [German version]({{< ref path="/meta/stats" lang="de" >}}) of the comic (*Nasenohren*).

* Number of comic strips in German: **{{< countcomics lang="de" >}}**
* German comic strips without translation: **{{< countexclusive lang="de" >}}**

## Global statistics
These stats take all comic strips of all language versions into account.

* Total number of comic strips in all languages: **{{< countcomics >}}**
* Last comic ID: **#{{< lastcomicid >}}**
* Size of all comic image files (not including previews): **{{< comicfilesize >}}**
