+++
date = "2021-11-02"
title = "Schrift"
+++

Dieser Comic verwendet eine eigene Schrift „**Nizzy Medium**“, die für die meisten Comics verwendet wird.

![Nizzy-Schriftvorschau](/assets/images/Nizzy_preview.png)

Merkmale:
* Nur Großbuchstaben
* Volle [ASCII](https://de.wikipedia.org/wiki/ASCII)-Unterstützung
* Volle [Latin-1](https://de.wikipedia.org/wiki/Unicodeblock_Lateinisch-1,_Erg%C3%A4nzung)-Unterstützung
* Großes Eszett
* Exklusiv: Das Symbol für Moneten, die Währung von Nasenohren (U+F006)
* Einige ausgewählte andere Zeichen

## Herunterladen

Aktuelle Version: {{< param nizzyfontversion >}}

* [Nizzy-Medium.ttf herunterladen](/assets/fonts/Nizzy-Medium.ttf)

## Namensnennung / Lizenz

Die Schrift ist ein Derivat und eine von Wuzzy erweiterte Version der Schrift „Nina Medium“ von Nina Paley.

Lizenz: [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
