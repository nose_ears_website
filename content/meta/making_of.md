+++
title = "Making-of"
date = "2021-11-09"
+++
The creation of the comics is simple but highly efficient.

I can’t draw well, so I have constructed a library of predrawn Mimi and Eunice characters. The library was constructed by extracting many character drawings from the “Mimi and Eunice” comics (which are [free cultural works](https://freedomdefined.org/Definition)), showing Mimi and Eunice in various different emotional states, each of which with multiple variants to avoid obvious repetitions. Building this library was tedious work, but it was worth it as it speeds up the creation of new comics massively.

For the text I mostly use the “Nizzy” [font]({{< ref "/meta/font" >}}), a derivative of the “Nina” font family by Nina Paley, or I use the Nina font family directly. I have edited the font myselves, mostly to add new glyphs.

The first step of every comic is thinking of a joke or scenario. And then move on actually creating it. To create an actual comic, I launch the [GIMP](https://www.gimp.org), load up one of various templates I’ve created, write the text, then draw speech bubbles, then put down the characters. Sometimes I add a few little extra images, these are mostly based on Public Domain imagery. But that’s not all. For each comic, I also have to write a bunch of metadata. Title, topics, translation, a transcription for people that can’t view images, etc.

## Tools used

Well, what can I say? I am truly standing on the shoulders of giants. I’m a huge [free software](http://wikipedia.org/wiki/Free_Software) nerd. The creation of this webcomic involves a lot of free software. Here’s a list of software (all of which is *free software*) that I use all the time for creating this comic:

* **Comic images**
    * The [GIMP](https://www.gimp.org), an image editor. This is the main program for making the comics. I have a lot of experience with this program since I used it for ages
    * [Inkscape](https://inkscape.org), a vector graphics editor. I rarely use it to edit several public domain vector graphics to better suit my comic needs, and then import them into the GIMP
    * [Blender](https://www.blender.org), a 3D modelling software. I have used it for *[Circumnavigation]({{< ref "/comic/372" >}})*
* **Text logo**
    * Inkscape
* **Font**
    * [FontForge](https://fontforge.org), a powerful font editor. I used it to draw new glyphs
* **Website**
    * [Hugo](https://gohugo.io), a static website generator. For laypeople: This is a tool which helps me automating the creation of every page of this website. Without it, I would have to create every single page by hand, and copy it again and again. As this website has hundreds of pages, you can imagine how fast it would be impossible to maintain. Hugo is very important
    * [Git](https://git-scm.com), a version control tool, a programmer’s all-time favourite. In laypeople’s terms, this is basically a tool which keeps tracks changes to all important files I make (for the website), like a giant change protocol. If ever screw up in my code, I can always go back.
* **Writing**
    * [Vim](https://www.vim.org) (mostly), an old-school text editor because I’m old-school.
* **Hosting**
    * This website is hosted on [Neocities](https://github.com/neocities), a hosting service for websites. Neocities itself released all their own software they used as free software as well because they’re awesome!

## History

The first comics actually come from late 2014 with my first early experiments of comic edits. Those early comics are not published, but the reworked versions of them are. Then, for many years, basically nothing happened.

In October 2021, I finally decided to turn my experiment into something serious and thus Nose Ears was born. I just felt like it. Many new comics were made, transcriptions were typed, the website was created, translations were made, and more.

On the 24th of October 24, 2021, the Nose Ears website went public.

And that’s roughly how Nose Ears came into being. For the long history, see [Long history]({{< ref "/meta/long_history" >}}).

