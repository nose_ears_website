+++
title = "Mitwirkende für Comic #263"
date = "2022-05-08"
+++

[Comic #263]({{< ref "/comic/263" >}}) verwendet ein Bild, das mit dem Event Horizon Telescope aufgenommen wurde, das berühmte allererste Bild eines schwarzen Lochs. Es wurde von der Europäischen Südsternwarte (ESO) erstellt. Es steht unter der „[Creative Commons Namensnennung 4.0 International](https://creativecommons.org/licenses/by/4.0/deed.de)“-Lizenz.

Auf deren Website heißt es (übersetzt): „Sofern nicht ausdrücklich vermerkt, stehen die auf der öffentlichen ESO-Webpräsenz verbreiteten Bilder, Videos und Musikstücke sowie die Texte von Pressemitteilungen, Ankündigungen, Bildern der Woche, Blogbeiträgen und Bildunterschriften unter einer ‚Creative Commons Namensnennung 4.0 International‘-Lizenz und dürfen auf nicht-exklusiver Basis ohne Gebühr reproduziert werden, sofern die Quellenangabe deutlich und sichtbar ist.“.

Bildquelle: [https://www.eso.org/public/images/eso1907a/](https://www.eso.org/public/images/eso1907a/) (Datum 8.5.2022)
