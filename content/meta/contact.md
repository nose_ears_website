+++
title = "Contact"
date = "2021-10-22"
+++

Here’s how you contact me, Wuzzy:

* **E-mail**: [Wuzzy@disroot.org](mailto:Wuzzy@disroot.org)
* **XMPP**: [Wuzzy2\@jabber.ccc.de](xmpp:Wuzzy2@jabber.ccc.de)
* **Mastodon**: [@Wuzzy@cyberplace.social](https://cyberplace.social/@Wuzzy)
* **Matrix**: \@wuzzy:matrix.org
* **IRC**: Wuzzy on [Libera.Chat](ircs://irc.libera.chat:6697)

I also have a [personal website](https://wuzzy.codeberg.page).
