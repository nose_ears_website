+++
date = "2021-11-02"
title = "Font"
+++

This comic uses the custom font “**Nizzy Medium**” which is used for making most of the comics.

![Nizzy font preview](/assets/images/Nizzy_preview.png)

Features:
* All-uppercase font
* Full [ASCII](https://en.wikipedia.org/wiki/ASCII) support
* Full [Latin-1](https://en.wikipedia.org/wiki/Latin-1_Supplement_(Unicode_block)) support
* German capital eszett
* Exclusive: The Money symbol, the currency of Nose Ears (U+F006)
* A few selected other characters

## Download

Current version: {{< param nizzyfontversion >}}

* [Download Nizzy-Medium.ttf](/assets/fonts/Nizzy-Medium.ttf)

## Attribution / license

The font is derivative work and an extended version (extended by Wuzzy) of the “Nina Medium” font by Nina Paley.

License: [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
