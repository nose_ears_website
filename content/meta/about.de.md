+++
title = "Über"
date = "2021-10-25"
+++
**Nasenohren** ist ein absurder Comic über die absurden Dinge im Leben. Es gibt kein bestimmtes Thema, denn die Absurdität lauert überall: Im [Internet]({{< ref "/topics/internet" >}}), in [Computern]({{< ref "/topics/computing" >}}), [Spielen]({{< ref "/topics/games" >}}), [Gender]({{< ref "/topics/gender" >}}), bei [Hass]({{< ref "/topics/bigotry" >}}), in [Machtsystemen]({{< ref "/topics/power" >}}), im [Urheberrecht]({{< ref "/topics/copyright" >}}), in der [Sprache]({{< ref "/topics/language" >}}), in [beschissenen Ideologien]({{< ref "/topics/nazi-crap" >}}), [Glaubenssystemen]({{< ref "/topics/religion" >}}), [Emotionen]({{< ref "/topics/emotions" >}}) und in [vielem mehr]({{< ref "/topics/" >}}).

Es gibt auch eine offizielle [englische Version]({{< ref path="/meta/about" lang="en" >}}). Ein paar Comics sind unübersetzbar und sind daher nur in einer Sprache vorhanden.

## Die Figuren

Siehe [Figuren]({{< ref "/meta/chars" >}}).

## Weiterführende Infos

* [Making-of]({{< ref "/meta/making_of" >}})
* [Statistiken]({{< ref "/meta/stats" >}})
