+++
title = "Mitwirkende für Comic #57"
date = "2020-10-29"
+++

[Comic #57]({{< ref "/comic/57" >}}) verwendet Logos von diversen
Softwareprojekten. Diese Logos sind allesamt freie kulturelle Werke
und wurden unter verschiedenen freien Lizenzen veröffentlicht. Die
Autoren und Lizenzen dieser Softwarelogos werden hier aufgelistet:

* Godot: Andrea Calabró (CC BY 3.0)
* Xonotic: Team Xonotic (GPLv2+)
* Hedgewars: Hedgewars Project (GPLv2)
* GIMP: Tuomas Kuosmanen (GPLv2+)
* Inkscape: Unbekannter Autor (CC BY-SA 3.0)
* CMake: CMake-Team (CC0 1.0)
* GNU: Aurelio A. Heckert <aurium@gmail.com> (CC BY-SA 2.0)
* OpenOffice.org: ™/®Sun Microsystems (LGPLv2.1+)
* Blender: ™/®Blender Foundation (zu simpel für Copyright)
* Krita: KDE (GPLv3)
* systemd: Tobias Bernard (zu simpel für Copyright)
* Linux-libre: © 2009 Rubén Rodríguez Pérez / FSFLA / GFDL 1.2
* OpenWrt: OpenWrt (CC BY-SA 4.0)
* Linux: lewing@isc.tamu.edu Larry Ewing und The GIMP (CC0 1.0)
* IceCat: Hitflip (GPLv2)
* Firefox: Mozilla (LGPLv2)
* FileZilla: eeme158-Team (GPLv2)
* VLC: Richard C. G. Øiestad (GPLv2+)
* Mumble: Martin Skilnand (CC BY-SA 3.0)
* JOSM: Diamond00744 (CC0 1.0)
* LibreOffice: Christoph Noack (CC BY-SA 3.0)
* KDE: KDE (LGPLv2.1+)
* LMMS: LMMO Community (CC0 1.0)
* GNOME: Sven, Bruce89 und Vulphere (LGPL 2.1+)
* XFCE: Xfce Team, Sav vas (LGPLv2.1+)
* Libreboot: Marcus Moeller (CC0 1.0)
* BigBlueButton: BigBlueButton, Inc. (für simpel für Copyright)
* Apache HTTP Server: The Apache Software Foundation (Apache Licence 2.0)
* MediaWiki: Serhio Magpie (CC BY-SA 4.0)
* OpenVPN: ™/®OpenVPN Inc. (too simple for copyright)

Ein Pluszeichen hinter dem Lizenznamen bedeutet, dass auch
spätere Versionen dieser Lizenz zulässig sind.

Links zu den Lizenzen:

* [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
* [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.de)
* [CC BY-SA 2.0](https://creativecommons.org/licenses/by/2.0/deed.de)
* [CC BY-SA 3.0](https://creativecommons.org/licenses/by/3.0/deed.de)
* [CC BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/deed.de)
* [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
* [LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html)
* [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)
