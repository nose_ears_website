+++
title = "Privacy Policy"
date = "2021-10-18"
+++
The Nose Ears website itself and the author of this website do **not** collect any data.

However, data may be collected by the organization that *hosts* this website, i.e. Neocities. Please refer to [Neocities’ Privacy Policy](https://neocities.org/privacy).
