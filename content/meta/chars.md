+++
title = "Characters"
date = "2024-01-30"
+++
The main characters are snow white circular-shaped little creatures. They have a nose, ears, eyes and feet. They are part of an unknown species, but they’re definitely not human.

## Identity

Their identity, role and mood change wildly all the time. They represent the nature of **chaos** itself, a blank slate, to be a mockery of making sense. 

One day, they might work as [prison guard]({{< ref "/comic/8" >}}) or for [Corporation, Inc.]({{< ref "/comic/25" >}}), another day they become brave warriors in the fight against the [real enemy]({{< ref "/comic/30" >}}), and in yet another day they just [argue about who ate the cake]({{< ref "/comic/64" >}}). They [love]({{< ref "/comic/71" >}}), [laugh]({{< ref "/comic/70" >}}), are [sad]({{< ref "/comic/23" >}}), [cry]({{< ref "/comic/62" >}}).

What’s for sure is that everyone has a lot of opinions on pretty much everything, only to make a 180° turn the other day. They often mock each other for bad ad-hoc arguments (or arguments that *seem* to be bad). More often than not, they get into a fight, only to find peace shortly afterwards. Their relationship status is as chaotic as their identities.

## Character list

### Mimi
![Mimi](/assets/images/mimi.png)

* [Pronunciation](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet): [ˈmimi]
* Ear shape: pointy-eared
* Gender: chaotic
* Pronoun: *any*
* First appearance: [Hate]({{< ref "/comic/1" >}})

### Eunice
![Eunice](/assets/images/eunice.png)

* Pronunciation: [ˈjunɜs]
* Ear shape: floppy-eared
* Gender: chaotic
* Pronoun: *any*
* First appearance: [Hate]({{< ref "/comic/1" >}})

### Poppy
![Poppy](/assets/images/poppy.png)

* Pronunciation: [ˈpopi]
* Ear shape: [shift-eared]({{< ref "/comic/123" >}})
* Gender: female
* Pronoun: she
* First appearance: [Ear Binary]({{< ref "/comic/53" >}})

### Keno
![Keno](/assets/images/keno.png)

* Pronunciation: [ˈkeno]
* Ear shape: Kink-eared
* Gender: male
* Pronoun: he
* First appearance: [Ear Ternary]({{< ref "/comic/123" >}})

## Grammar

You may wonder: “What does ‘Pronoun: any’ mean?”. Well, it means the pronouns used to refer to the person could be anything that is grammatically correct. So it can be “he”, “she” or even “it”, “they”, “xer” or whatever else can be used as pronoun nowadays.

For example, all of these statements mean the same:

* “Mimi is an agent of chaos. *She* doesn’t mind.”
* “Mimi is an agent of chaos. *He* doesn’t mind.”
* “Mimi is an agent of chaos. *They* don’t mind.”
* “Mimi is an agent of chaos. *Xer* doesn’t mind.”

But the identity chaos has a limit: Once a pronoun was picked, a character’s pronoun never changes in the middle of a comic strip. ;-)

Poppy and Keno are different, their pronouns is well-defined, actually. It’s always “he” for Keno and always “she” for Poppy.

## The world of Nose Ears

### Corporation, Inc.
![Corporation, Inc. logo](/assets/images/corporation_inc_logo_en.png)

**Corporation, Incorporated** isn’t a character but keeps appearing from time to time. It is the giant faceless multi-quadrillion company that seeks to dominate the market and by extension, the world. They are mainly active as a software and Internet company, but, being a giant faceless multi-quadrillion company, they occasionally try to enter other markets as well. Their logo is the capital letter C.

### Currency

The official currency is called “money”. The plural of “money” is “monies”. The currency symbol is the capital letter M with one dash at each of the left and right side.

## Further info

* [About]({{< ref "/meta/about" >}})
