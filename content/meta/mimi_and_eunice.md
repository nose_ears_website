+++
title = "Mimi and Eunice"
date = "2021-10-21"
draft = true
+++
**Mimi and Eunice** is the name of the webcomic from which Nose Ears is derived.

It is still one of my most favourite webcomics. It is

## The Jenndra Identitty era

Jenndra Identitty is a new character that Nina introduced after she was supposedly cancelled from an event for supposed transphobic views. I don’t know the full story, nor do I know whether the cancellation was justified or an overreaction. But this does not matter here. Let’s give Nina the benefit of the doubt *for now*.

Jenndra is a caricature of an evil trans rights activists who abuse trans rights to oppress people and in general just be a dick to people. Pun intended, because Jenndra literally looks like a dick. Jenndra Identity is supposedly a trans women, or, in transphobe language, a “trans-identified woman”. The name is also obviously a pun on “gender identity”. Jenndra always plays the role of the asshole in the comics. This in itself is not the problem, but it is the whole identity of Jenndra.

The whole character itself is already extremely offensive because the dick shape. It implies that trans women are not women but actually men. It implies that the existence of a penis means you have to be a man, and there is nothing you can do to change this, no matter what you do. Jenndra basically serves to push for biological essentialism by making the mere concept of gender identity the butt of the joke. Unfortunately, the joke’s not working if you actualy know things about the LGBTQ+.

The fundamental flaw with Jenndra is the implication that Jenndra is not a "real" women, only "identifying" as one. **This is transphobia by definition. Saying that trans women are not women is transphobia by definition.** Note that this is not just a technicality or a cheap "gotcha" moment. This is THE defining characteristic of transphobia, and there is no way around that. You can say you "respect" trans people all you want, or claim that you love trans people as much you like. Pat yourselves on the shoulder for being so "tolerant", but you’re still a fool. Because as long you do not respect their identity, you’re being transphobic. Again, transpobic *by definition*. There is nothing to interpret here, nothing to guess.

And then there is just the whole content of the Jenndra comics which is not better. Jenndra is consistently always being the villain, the asshole, the adversary in these comics. There is nothing inherently wrong if trans people get to be the "evil ones" in stories. Because trans people are people like everyone else, they also can be evil or good like everyone else. Saying that trans people could never do harm is as nonsensical as saying they are inherently evil.

So, the question is that, why is a trans character portrayed as evil. Some unrelated ideology or backstory? Or their trans identity itself? But here’s out problem: Jenndra is portrayed as evil specifically *because* she is trans. That alone makes the whole Jenndra series transphobic as hell.

### Subverting Jenndra?

At first I was very tempted to just subvert the Jenndra comics by rewriting the story. After all, why not turning the worst comics of Mimi and Eunice into something positive?
I even had some ideas prepared. But later discarded them. The reason is that the whole Jenndra character itself is a hateful caricature of a supposed evil trans activist stereotype. The whole character screams "I’m a fake! I’m not a REAL woman.". In short, the transphobia is written into her DNA from day one. So no matter how much I try to subvert the dialogues or try to make it trans-positive, the initial transphobia will still remain. I don’t think it is even possible to even parody Jenndra without reproducing the original transphobia, unless I change the Jenndra character so much that it’s basically a new character.
Nina Paley also said herself that being offensive was the point.

Therefore, I decided there will be NO inclusion of Jenndra at all.
