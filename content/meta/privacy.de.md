+++
title = "Datenschutz"
date = "2021-10-25"
+++
Die Nasenohren-Webpräsenz an sich und der Autor derselben sammeln **keine** Daten.

Jedoch könnten Daten von der Organisation, welche diese Webseite *hostet*, gesammelt werden, d.h. Neocities. Siehe die [„Privacy Policy“ (also Datenschutzrichtlinie) von Neocities](https://neocities.org/privacy) (leider nur auf Englisch).
