+++
title = "Credits for comic #263"
date = "2022-05-08"
+++

[Comic #263]({{< ref "/comic/263" >}}) uses an image captured by the Event Horizon Telescope, the famous first ever image of a black hole. It was created by the European Southern Observatory (ESO). It is licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) license.

Their website states: “Unless specifically noted, the images, videos, and music distributed on the public ESO website, along with the texts of press releases, announcements, pictures of the week, blog posts and captions, are licensed under a Creative Commons Attribution 4.0 International License, and may on a non-exclusive basis be reproduced without fee provided the credit is clear and visible.”.

Image source: [https://www.eso.org/public/images/eso1907a/](https://www.eso.org/public/images/eso1907a/) (Date: 8th of May, 2022)
