+++
title = "About"
date = "2021-10-18"
+++
**Nose Ears** is an absurd comic about the absurd things in life. There’s not any particular topic because absurdity can be found everywhere: In the [Internet]({{< ref "/topics/internet" >}}), in [computers]({{< ref "/topics/computing" >}}), [games]({{< ref "/topics/games" >}}), [gender]({{< ref "/topics/gender" >}}), [bigotry]({{< ref "/topics/bigotry" >}}), [power structures]({{< ref "/topics/power" >}}), [copyright]({{< ref "/topics/copyright" >}}), [language]({{< ref "/topics/language" >}}), [crappy ideologies]({{< ref "/topics/nazi-crap" >}}), [belief systems]({{< ref "/topics/religion" >}}), [emotions]({{< ref "/topics/emotions" >}}) and [much more]({{< ref "/topics/" >}}).

There’s also an official [German version]({{< ref path="/meta/about" lang="de" >}}). A few comic strips are untranslatable, so they only exist in one language.

## The characters

See [Characters]({{< ref "/meta/chars" >}}).

## Further info

* [Making-of]({{< ref "/meta/making_of" >}})
* [Statistics]({{< ref "/meta/stats" >}})
