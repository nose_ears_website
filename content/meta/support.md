+++
title = "Support this comic!"
date = "2021-10-22"
+++

Do you like this comic a lot? Here’s how you can support it:

## Visibility support

The easiest way to support this comic is to simply **spread the word**. Tell your friends, post links, post some comic strips on discussion boards, in chat, print it out, be creative. Art is created to be seen, so the more eyeballs see this, the better. Don’t worry about copyright, this comic is a free cultural work. See [here]({{< ref "/meta/credits" >}}) for details.

### No hotlinking!

Note that hotlinking (i.e. directly linking to the image file to embed it in a webpage) is currently *not* supported. If you want to share a comic, either share the link, or copy the image directly.

## Vote on Top Web Comics

Nose Ears is listed on [Top Web Comics](https://www.topwebcomics.com/). You can vote for Nose Ears [here](https://www.topwebcomics.com/vote/27297?userid=Wuzzy) every 24h to help it rise in the rankings.
