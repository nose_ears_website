+++
title = "Long history"
date = "2021-10-21"
+++
This is how Nose Ears came into being.

## Mimi and Eunice
I always was a big fan of Mimi and Eunice by Nina Paley, which is one of my favourite webcomics. I like the simplistic structure, and the fact that it is one of the very few webcomics directly attacking the talking poins of copyright maximalists. I have followed it for years and without it, Nose Ears would not exist. Nina Paley obviously deserves a lot of credit for this, although she is not directly involved in the making of Nose Ears at any point.

## 2014: Early comics
Technically, Nose Ears actually dates back to late 2014, when I started to make the first comics, derived from Mimi and Eunice comics. I experienting around with the “Mimi and Eunice” webcomic; I also was obsessed with Lojban back then so I made the first comics in Lojban, and only later made English versions of them. Comics [#1]({{< ref "/comic/1" >}}) to [#18]({{< ref "/comic/18" >}}) (except [#9]({{< ref "/comic/9" >}})) were originally created in that time. The method I used to make the comics back then was slightly different: I actually drew the letters myself. I have since dropped this practice nowadays since it’s too tedious to do. Now I simply use a font.

My “library” of character drawings (images of Mimi and Eunice) also dates back to the earliest days. Since there are over 500 Mimi and Eunice comics to choose from, I have many “Mimis” and “Eunices” to choose from for building new comics. It’s a simple but genius trick to make every comic appear unique. I want to avoid repeating identical character drawings, or at least making it too obvious. Having the characters appear in different emotions and in various variants is important to me.

## 2014-2021: The Era of Nothing
Then, my comics laid dormant on my harddrive for years and pretty much nothing happened. I occassionally had the idea to just upload the comics somewhere but without a proper website, the best I could do would only be a boring list. This wasn’t good enough for me.

In the meanwhile, the “Mimi and Eunice” comic has basically died (it is dead to *me*, anyway), with no update since years, and and the latest comics have become very transphobic by introducing a new character that was basically a very bad stereotype. I might write more about later for *why* exactly I think these comics are transphobic because it isn’t obvious for laypeople.

Yet, I still think Mimi and Eunice overall is a brilliant webcomic which elegantly exposes the copyright regime. This is the area in which this webcomic shines. When it comes to gender, it’s trash. Mimi and Eunice didn’t start transphobic, it became transphobic at the very end.

## October 2021: Nose Ears begins!
In October 2021, I then decided to make new comics featuring the Mimi and Eunice characters. I don’t really know *why* I decided it, I think I just felt like it. ;-)

The first obvious idea was to create some trans-positive comics, to directly counter the transphobic nonsense in the same way Nina so expertly exposed nonsensical copyright logic. This is why there are many comics about queerness in the beginning. But Nose Ears is meant to be more than that. I will cover all sorts of topics that interest me, mainly computing, free software, unjust power structures, making fun of absurd arguments.

So I began creating a ton of new comics and was very productive in only about a week or so. In this month, I also created the beautiful Nose Ears website completely from scratch by using [Hugo](https://gohugo.io/), a static website generator.

I also reworked all my ancient comics from 2014, because they did not satisfy me yet. Since those comics weren’t released anywhere yet, this was fine. Also, I put a lot of work in increasing and fixing my character library. Many of the old character drawings I extracted had annoying mistakes which I could not tolerate, so I started fixed them.

I also create German counterparts of the comics. For this, the “Nina” font family I use is not enough as it doesn’t support the additional German characters I need (umlauts, eszett, quotation marks), so I edited the [font]({{< ref "/meta/font" >}}) and created the missing glyphs to support German.

I also had to think of a new name. “Mimi and Eunice 2” came into mind at first but I had to discard this. My comic isn’t meant as a continuation, but as its own independent thing. Also, including the other comic’s name in the title just gives false associations. So I needed a completely new unique name, and after a bit of thinking, I took “Nose Ears”. It’s “Nose Ears” because those are the two defining body parts of Mimi and Eunice. And it just sounds very funny, especially in German (“Nasenohren”).

And finally, on the 24th of October 24, 2021, the Nose Ears website went public, starting with 66 comics.

## Further events

* 2nd February, 2022: [Stats page]({{< ref "/meta/stats" >}}) added
* 3rd April, 2023: Support for small screens added
* 7th August, 2023: Comic list also shows the comic ID and date
* 7th September, 2023: [Archive](https://archive.org/details/nose-ears-archive) published
* 2nd February, 2024: Added Nose Ears to [Top Web Comics](https://www.topwebcomics.com/sample/27297), [Comic Rocket](https://www.comic-rocket.com/explore/nose-ears/), [Belfry Comics](https://new.belfrycomics.net/), [Webcomic-Verzeichnis](http://www.webcomic-verzeichnis.de/comic.php?comic=440), [Piperka](https://piperka.net/info.html?cid=10205) and [Archive Binge](https://archivebinge.com/comic/1733)
* 19th February, 2024: Topic list below comic now has links to previous and next comic of each topic
* 19th February, 2024: Organized some comics into [series](/series)
