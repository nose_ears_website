+++
title = "Credits"
date = "2021-10-18"
+++
**Nose Ears** is a webcomic created by Wuzzy ([personal website](https://wuzzy.codeberg.page)).

Please copy and share! ♡ I want this comic to spread.

Nose Ears is a [free cultural work](https://freedomdefined.org/Definition).
You are encouraged to copy and share it, make
derivative works of it, and share those derivative works. Whether commercially or
not, as long you you get the attribution right and don’t violate the
copyleft requirement of the license, it’s fine.

## Credits

* Stories, text: Wuzzy
* English and German language versions: Wuzzy
* Website: Wuzzy
* Text logo: Wuzzy (by using the “Nina” font family by Nina Paley)
* Character drawings: Nina Paley (sometimes with modifications by Wuzzy, see below)
* Other images/drawings: Anonymous authors (Public Domain sources), Nina Paley, Wuzzy
* [Font]({{< ref "/meta/font" >}}): Nina Paley, Wuzzy

## License / legal information

Nose Ears is licensed under **CC BY-SA 3.0**, i.e. 
[Creative Commons Attribution Share-Alike 3.0 Unported](https://creativecommons.org/licenses/by-sa/3.0/).

[Comic #57]({{< ref "/comic/57" >}}) uses free/libre logos from various free software projects.
See [Credits for comic #57]({{< ref "/meta/credits_57" >}}) for details.

[Comic #263]({{< ref "/comic/263" >}}) uses an image of the European Southern Observatory (ESO).
See [Credits for Comic #263]({{< ref "/meta/credits_263" >}}) for details.

Nose Ears is a derivative work of the webcomic Mimi and Eunice by Nina Paley.
Mimi and Eunice is also licensed under CC BY-SA 3.0. Nose Ears
features both titular characters (Mimi, Eunice) as well the
drawings of those characters. Additional characters like Poppy and Keno
are new and based on the “Mimi and Eunice” drawings. The stories and
texts in Nose Ears are completely new.

Other drawings and images are a mix of images derived from the Mimi and Eunice
webcomic, images from various anonymous public domain sources, and rarely
Wuzzy’s own creations.

Although Nina Paley’s work is used for Nose Ears, she is *not* actively involved
in the creation of Nose Ears. Nose Ears is being created completely independently
of Mimi and Eunice. There is no direct collaboration at all, nor an endorsement.

Also, the “Nina” font family by Nina Paley is used for
the text (including the text logo), licensed under
CC BY-SA 3.0. The font family was obtained from
[archive.org](https://archive.org/details/NinaPaleyFonts9).
But for most comics, I now use an edited version of the font, see [Font]({{< ref "/meta/font" >}}).

### Evidence of obtained licenses

Evidence of Mimi and Eunice license:

* [Mimi and Eunice about page](https://mimiandeunice.com/about/)
* [Archived Mimi and Eunice About page](https://web.archive.org/web/20210809081608/https://mimiandeunice.com/about/)
* [Archived screenshot](/assets/images/Mimi_and_Eunice_permission_CC_BY_SA_3_0.png) (taken at Oct 18, 2021)

Evidence of Nina font family license:

* [Info page on archive.org](https://archive.org/details/NinaPaleyFonts)
* [Archived screenshot](/assets/images/Nina_font_permission_CC_BY_SA_3_0.png) (taken at Oct 18, 2021)
