#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Run this script to generate the comic .md files from the CSV files
# and put them into content/comic.

import csv
csvfile_main = open('./comic_data.csv', newline='')
if csvfile_main == None:
	print('Could not load main CSV file!')
	exit()

langs = ['en', 'de']
csvfiles_lang = {}
for lang in langs:
	csvfile = open('./comic_data_'+lang+'.csv', newline='')
	if csvfile == None:
		print('Could not load language CSV file!')
		exit()
	csvfiles_lang[lang] = csvfile

for lang in langs:
	# Re-open main file to reset read/write pointer
	# TODO: Properly use seek function...
	csvfile_main = open('./comic_data.csv', newline='')
	if csvfile_main == None:
		print('Could not re-open main CSV file!')
		exit()

	table = []
	reader_main = csv.reader(csvfile_main, delimiter=',', quotechar='"')
	for row in reader_main:
		_id_main = row[0]
		topics = row[1]
		creationDate = row[2]
		publishDate = row[3]
		# The version of the comic, in case a comic needs updating
		# The first version is always 1 and is incremented later.
		version = row[4]
		series = row[5]

		csvfile_lang = csvfiles_lang[lang]
		reader_lang = csv.reader(csvfile_lang, delimiter=',', quotechar='"')
		for row_lang in reader_lang:
			_id_lang = row_lang[0]
			if _id_main == _id_lang:
				if (len(row_lang) < 4):
					print('Insufficient columns in line '+_id_main+' (lang='+lang+')')
					exit()

				title = row_lang[1]
				altText = row_lang[2]
				authorComment = row_lang[3]
				table.append([_id_main, topics, creationDate, publishDate, version, title, altText, authorComment, series])
				break
	print (lang)
	for row in table:
		_id = row[0]
		topics = row[1]
		topics_t = topics.replace("|", "\",\"")
		if topics_t != "":
			topics_t = "\"" + topics_t + "\""
		creationDate = row[2]
		publishDate = row[3]
		version = row[4]
		title = row[5]
		altText = row[6]
		authorComment = row[7]
		series = row[8]

		if title != "___UNUSED___":
			out = ""
			out += "+++\n"
			out += "weight= \"" + _id + "\"\n"
			out += "title = \"" + title + "\"\n"
			out += "topics = [" + topics_t + "]\n"
			out += "series = \"" + series + "\"\n"
			out += "creationDate = \"" + creationDate + "\"\n"
			out += "date = \"" + publishDate + "\"\n"
			out += "publishDate = \"" + publishDate + "\"\n"
			out += "comic_version = \"" + version + "\"\n"
			if altText != "" and altText != None:
				out += "alt_text = \"" + altText + "\"\n"
			if authorComment != "" and authorComment != None:
				out += "author_comment = \"" + authorComment + "\"\n"
			out += "+++"

			if lang != 'en':
				lang_tag = '.'+lang
			else:
				lang_tag = ''
			with open("./content/comic/"+_id+lang_tag+".md", "w", encoding='utf-8') as file:
				file.write(out)

	csvfile_lang.close()
	csvfile_main.close()

